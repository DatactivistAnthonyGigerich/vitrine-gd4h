# Accessibilité : non conforme

Ce site n'a pas été soumis à un audit de conformité permettant de mesurer le respect des critères <a href="https://accessibilite.numerique.gouv.fr/" target="_blank">RGAA 4.1<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>.


Bien que temporaire, une attention particulière a tout de même été portée lors de la conception et du développement de ce site afin de respecter au mieux les critères d'accessibilité ainsi que du <a href="https://www.systeme-de-design.gouv.fr/" target="_blank">système de design de l'état<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>.

### Retour d’information et contact 
 
Si vous n’arrivez pas à accéder à un contenu ou à un service, vous pouvez contacter le responsable 
du site web pour être orienté vers une alternative accessible ou obtenir le contenu sous une autre 
forme : 
 
Par mail : <a href="mailto:hello@datactivist.coop" target="_blank">hello@datactivist.coop</a>
 
### Voies de recours 
 
Cette procédure est à utiliser dans le cas suivant : 
 
Vous avez signalé au responsable du site web un défaut d’accessibilité qui vous empêche d’accéder 
à un contenu ou à un des services et vous n’avez pas obtenu de réponse satisfaisante. 
 
- Écrire un message au Défenseur des droits (<a href="https://formulaire.defenseurdesdroits.fr" target="_blank">https://formulaire.defenseurdesdroits.fr<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>)
- Contacter le délégué du Défenseur des droits dans votre région (<a href="https://www.defenseurdesdroits.fr/saisir/delegues" target="_blank">https://www.defenseurdesdroits.fr/saisir/delegues<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>)
 
- Envoyer un courrier par la poste (gratuit, ne pas mettre de timbre) :<br>Défenseur des droits<br>Libre réponse 71120<br>75342 Paris CEDEX 07 
