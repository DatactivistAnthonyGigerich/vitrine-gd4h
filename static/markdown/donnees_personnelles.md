# Données personnelles et cookies

Le site <a href="https://gd4h.ecologie.gouv.fr/" target="_blank">gd4h.ecologie.gouv.fr<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a> est soumis au Règlement général sur la protection des données 2016/679 (RGPD) et à la Loi Informatique et Libertés n°78-17.

## Traitements des données à caractère personnel
Pour exercer vos droits d'accès, de rectification, de limitation et d’effacement des données personnelles vous concernant, merci d'envoyer un email à cette adresse : <a href="green-data-for-health@developpement-durable.gouv.fr" target="_blank">green-data-for-health@developpement-durable.gouv.fr<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>

## Cookies

Lors de la consultation du forum <a href="https://challenge.gd4h.ecologie.gouv.fr" target="_blank">https://challenge.gd4h.ecologie.gouv.fr<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>, des témoins de connexion, dits « cookies », sont déposés sur votre ordinateur, votre mobile ou votre tablette. Lors de votre première visite, un bandeau signale leur présence.

#### Matomo

L’outil de mesure d’audience déployé sur ce forum est <a href="https://stats.challenge.gd4h.ecologie.gouv.fr/index.php?idSite=1" target="_blank">Matomo<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>. Matomo est soumis à la loi Informatique et Libertés.

Ces cookies permettent aux gestionnaires du site d’établir des mesures statistiques de fréquentation et d’utilisation du site :
- le cookie déposé sert uniquement à la production de statistiques anonymes
- le cookie ne permet pas de suivre la navigation de l’internaute sur d’autres sites que celui du site challenge.gd4h.ecologie.gouv.fr

#### Youtube

Certaines pages de ce site intègre des vidéos provenant de Youtube, des cookies nécessaires au fonctionnement de Youtube sont utilisés lors du lancement d'une vidéo. 

## Paramétrage des cookies
Vous pouvez paramétrer votre navigateur afin qu’il vous signale la présence de cookies et vous propose de les accepter ou non. Vous pouvez accepter ou refuser les cookies au cas par cas ou bien les refuser une fois pour toutes. A noter, ce paramétrage est susceptible de modifier vos conditions d’accès aux services du site nécessitant l’utilisation de cookies. Le paramétrage des cookies est différent pour chaque navigateur et en général décrit dans les menus d’aide.

## Les données sont collectées par :

Commissariat Général au Développement Durable

Tour Séquoia 92055

La Défense Cedex

Directeur de publication : Le Commissaire Général au Développement Durable

## En savoir plus sur la réglementation
<a href="https://www.cnil.fr/fr/reglement-europeen-protection-donnees" target="_blank">Le règlement général sur la protection des données (RGPD)<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>

<a href="https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000886460/" target="_blank">La loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés<span class="sr-only">ouverture d'une nouvelle fenêtre</span></a>