# Plan du site

- <a href="." target="_self">Accueil</a>
- <a href="defis" target="_self">Défis</a>
- - <a href="defi" target="_self">Défi</a>
- <a href="reglement" target="_self">Règlement</a>
- <a href="accessibilite" target="_self">Accessibilité</a>
- <a href="mentions_legales" target="_self">Mentions légales</a>
- <a href="donnees_personnelles" target="_self">Données personnelles et cookies</a>
- <a href="discourse_cookies" target="_self">Forum Discourse</a>