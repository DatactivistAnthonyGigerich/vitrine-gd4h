import { FetchError } from "./errors.js"
import fetchRetry from "fetch-retry"

export async function fetchJson(url, context) {
	const fetchWithRetry = fetchRetry(context.fetch, {
		retryOn: (attempt, error, response) => error !== null || !response.ok,
		retries: 6,
		retryDelay: (attempt, error, response) => (attempt + 1) * 500, // 500, 1000, 1500
	})
	const info = { url }
	let response
	try {
		response = await fetchWithRetry(url)
		if (!response.ok) {
			throw response
		}
	} catch (error) {
		throw new FetchError({
			message: `Error while fetching URL`,
			info,
		}).withCause(error)
	}
	try {
		return await response.json()
	} catch (error) {
		throw new FetchError({
			message: "Error while parsing JSON",
			info,
		}).withCause(error)
	}
}

export async function fetchText(url, context) {
	const fetchWithRetry = fetchRetry(context.fetch, {
		retryOn: (attempt, error, response) => error !== null || !response.ok,
		retries: 6,
		retryDelay: (attempt, error, response) => (attempt + 1) * 500, // 500, 1000, 1500
	})
	const info = { url }
	let response
	try {
		response = await fetchWithRetry(url)
		if (!response.ok) {
			throw response
		}
	} catch (error) {
		throw new FetchError({
			message: `Error while fetching URL`,
			info,
		}).withCause(error)
	}
	try {
		return await response.text()
	} catch (error) {
		throw new FetchError({
			message: "Error with response.text()",
			info,
		}).withCause(error)
	}
}
