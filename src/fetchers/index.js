export * from "./errors.js"

import * as discourse from "./discourse.js"
import { ConfigError } from "./errors.js"
import * as gitlab from "./gitlab.js"
import * as html from "./html.js"
import * as markdown from "./markdown.js"

const fetchers = { discourse, gitlab, html, markdown }

export function loadFetcher(sourceType) {
	const fetcher = fetchers[sourceType]
	if (!fetcher) {
		throw new ConfigError({
			message: `Source type "${sourceType}" not supported`,
			info: { sourceType },
		})
	}
	return fetcher
}

export async function fetchPageData(sourceRenderType, fetcher, source, config, context, query) {
	if (sourceRenderType === "cards") {
		return await fetcher.fetchCardsPageData(source, config, context, query)
	} else if (sourceRenderType === "content") {
		return await fetcher.fetchContentPageData(source, config, context, query)
	}
}
