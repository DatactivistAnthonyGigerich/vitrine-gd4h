import { ObjectModel } from "objectmodel"

import { justOneIsDefined } from "../utils"
import { PositiveInteger } from "./common"

const Source = new ObjectModel({
	type: String,
})

export const SourceDiscourseOrSourceGitlab = Source.extend({
	url: String,
	limit: [PositiveInteger],
	cardsPerLine: [PositiveInteger],
	cardsPerLineSm: [PositiveInteger],
	disableBlur: [Boolean],
	disableReadMore: [Boolean],
}).defaultTo({
	cardsPerLine: 4,
	cardsPerLineSm: 3,
	disableBlur: false,
	disableReadMore: false,
})

export const SourceDiscourse = SourceDiscourseOrSourceGitlab.extend({
	type: "discourse",
	category: [PositiveInteger],
	topic: [PositiveInteger],
	filterTag: [String],
})
	.assert((s) => justOneIsDefined([s.category, s.topic]), "only category or topic should be defined")
	.as("SourceDicourse")

export const SourceGitlab = SourceDiscourseOrSourceGitlab.extend({
	type: "gitlab",
	group: String,
}).as("SourceGitlab")

export const SourceHtml = Source.extend({
	type: "html",
	content: [String],
	file: [String],
	raw: [Boolean],
})
	.defaultTo({ raw: false })
	.assert((s) => justOneIsDefined([s.content, s.file]), "only content or file should be defined")
	.as("SourceHtml")

export const SourceMarkdown = Source.extend({
	type: "markdown",
	file: [String],
}).as("SourceMarkdown")
